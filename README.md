Dependencies
============

* python
* selenium

    pip install selenium

* firefox-geckodriver

    apt install firefox-geckodriver

Installation
============

Set cron job, e.g., every 10 minutes:

    */10 * * * * /opt/autologin/autologin.sh